<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend/index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/posts', 'PostController@index')->name('post.index');

//Route::get('/{anypath}', 'HomeController@index')->where('path','.*');

Route::group(['middleware' => ['auth']], function () {
    //Category
    Route::post('/add-category', 'CategoryController@store')->name('addCategory');
    Route::get('/category', 'CategoryController@all_category');
    Route::get('/category/{id}', 'CategoryController@destroy');
    Route::get('editcategory/{id}', 'CategoryController@edit');
    Route::post('update-category/{id}', 'CategoryController@update');
    //For_Post
    Route::get('/post', 'PostController@index');
    Route::post('/savepost', 'PostController@savepost');
    Route::get('/delete/{id}', 'PostController@delete_post');
    Route::get('/post/{id}', 'PostController@edit_post');
    Route::post('/update/{id}', 'PostController@update_post');
});

//******* For Frontend *******//
Route::get('/blog', 'BlogController@index');
Route::get('/singlePost/{id}', 'BlogController@singlePost');
Route::get('/listCategory', 'BlogController@categoryList');
Route::get('/sidebarCategory/{id}', 'BlogController@sidebarCategory');
Route::get('/search', 'BlogController@search');
Route::get('/recentPost', 'BlogController@recentPost');



