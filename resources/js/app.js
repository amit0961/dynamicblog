require('./bootstrap');
window.Vue = require('vue');
// Support vuex
import Vuex from 'vuex'
Vue.use(Vuex)

import storedata from "./store/index"
const store = new Vuex.Store(
    storedata

)
// vue router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import {routes} from "./routes";
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('admin-main', require('./components/Admin/AdminMaster.vue').default);
Vue.component('frontend-main', require('./components/Frontend/FrontendMaster.vue').default);

// v-form
import { Form, HasError, AlertError } from 'vform'

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
window.Form =Form;
//SweetAlert
import Swal from 'sweetalert2'
window.Swal =Swal;
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000
})
window.Toast =Toast;

//VueJs MarkDown Editor
import 'v-markdown-editor/dist/index.css';
import Editor from 'v-markdown-editor'
Vue.use(Editor);

//MomentJs Applied
import {filter} from './filter'


const router = new VueRouter({
    routes,
    mode:'hash'
   // short for `routes: routes`
});

const app = new Vue({
    el: '#app',
    router,
    store
});
