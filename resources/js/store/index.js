export default {
    state: {
        category: [],
        post: [],
        blog: [],
        singlepost:[],
        listCategory:[],
        postRecent:[],
    },
    getters:{
        getCategory(state){
            return state.category
        },
        getPost(state){
            return state.post
        },
        getBlog(state){
            return state.blog
        },
        singlepost(state){
            return state.singlepost
        },
        listCategory(state){
            return state.listCategory
        },
        postRecent(state){
            return state.postRecent
        },

    },
    actions:{
        allCategory(context){
            axios.get('/category')
                .then((response)=>{
                    context.commit('categories', response.data.categories)
                })
        },
        allPost(context){
            axios.get('/post')
                .then((response)=>{
                    console.log(response.data)
                    context.commit('postAll', response.data.posts) //this gonna apply on mutations
                })
        },
        allBlog(context){
            axios.get('/blog')
                .then((response)=>{
                    // console.log(response.data)
                    context.commit('blogAll', response.data.posts) //this gonna apply on mutations
                })
        },
        allsinglePost(context,payload){
            axios.get('/singlePost/'+payload)
                .then((response)=>{
                    // console.log(response.data)
                    context.commit('postSingle', response.data.post) //this gonna apply on mutations
                })
        },
        allCategoryList(context){
            axios.get('/listCategory')
                .then((response)=>{
                    context.commit('categoryList', response.data.categories) //this gonna apply on mutations
                })
        },
        getPostByCategoryId(context,payload){
            axios.get('/sidebarCategory/'+payload)
                .then((response)=>{
                    console.log(response.data)
                    context.commit('sidebarCategoryList', response.data.posts) //this gonna apply on mutations
                })
        },
        SearchPost(context,payload){
            axios.get('/search?s='+payload) //in here "s" act like as input value
                .then((response)=>{
                    context.commit('postSearch', response.data.posts) //this gonna apply on mutations
                })
        },
        recentPost(context){
            axios.get('/recentPost')
                .then((response)=>{
                    context.commit('latestPost', response.data.posts) //this gonna apply on mutations
                })
        },

    },
    mutations :{
        categories(state,data){
            return state.category = data
        },
        postAll(state,data){
            return state.post = data
        },
        blogAll(state,data){
            return state.blog = data
        },
        postSingle(state,payload){
            return state.singlepost = payload
        },
        categoryList(state,payload){
            return state.listCategory = payload
        },
        sidebarCategoryList(state,payload){
             state.blog = payload
        },
        postSearch(state,payload){
             state.blog = payload
        },
        latestPost(state,data){
            return state.postRecent = data
        },


    }
}
