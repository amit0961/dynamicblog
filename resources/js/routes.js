import AdminHome from "./components/Admin/AdminHome";

import CategoryList from "./components/Admin/Category/List";
import ExampleComponent from "./components/ExampleComponent";
import AddCategory from "./components/Admin/Category/New";
import EditCategory from "./components/Admin/Category/Edit";

//For_Post<<<<<
import PostList from "./components/Admin/Post/List";
import AddPost from "./components/Admin/Post/New";
import EditPost from "./components/Admin/Post/Edit";


//****   Frontend   *****//
import FrontendHome from "./components/Frontend/FrontendHome";
import BlogPost from "./components/Frontend/Blog/BlogPost";
import SinglePost from "./components/Frontend/Blog/SinglePost";




export const routes = [{
    path: '/home',
    component: AdminHome
},
    {
    path: '/posts',
    component: ExampleComponent
},
    {
        path: '/category-list',
        component: CategoryList
    },
    {
        path:'/add-category',
        component:AddCategory
    },
    {
        path:'/edit-category/:categoryid',
        component:EditCategory
    },

    //For_POST<<<<<<<<<<<<<
    {
        path: '/post-list',
        component: PostList
    },
    {
        path:'/add-post',
        component:AddPost
    },
    {
        path:'/edit-post/:postid',
        component:EditPost
    },




  //*****  For Frontend  *****//
    {
        path:'/',
        component:FrontendHome
    },
    {
        path:'/blog',
        component:BlogPost
    },
    {
        path:'/blog/:id',
        component:SinglePost
    },
    {
        path:'/categories/:id',
        component:BlogPost
    },

];
